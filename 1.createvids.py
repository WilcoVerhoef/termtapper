# This script generates a vids/cube_x_y_side.mp4 file for each cube side.
# All of these will be 11 by 11 black&white videos.
# (make sure that wavetapper.mp4 exists; the original wavetapper video)


from subprocess import run

FILENAME = "wavetapper.mp4"
OUTPUT_BW = "wavetapper-bw.mp4"
OUTPUT_BWL = "wavetapper-bw-light.mp4"

# CREATE BLACK-WHITE VERSION (WITH THRESHOLD 606060)
run([
    "ffmpeg",
    "-i", FILENAME,
    "-f", "lavfi", "-i", "color=0x606060:s=720x720",
    "-f", "lavfi", "-i", "color=black:s=720x720",
    "-f", "lavfi", "-i", "color=white:s=720x720",
    "-lavfi", "threshold", OUTPUT_BW
    ])

# CREATE BLACK-WHITE VERSION (WITH THRESHOLD 404040)
run([
    "ffmpeg",
    "-i", FILENAME,
    "-f", "lavfi", "-i", "color=0x404040:s=720x720",
    "-f", "lavfi", "-i", "color=black:s=720x720",
    "-f", "lavfi", "-i", "color=white:s=720x720",
    "-lavfi", "threshold", OUTPUT_BWL
    ])

ISO_CELL_WIDTH = 720 / 128
ISO_CELL_HEIGHT = 720 / 112

for j in range(4):
    for i in range(4):
        # center vertex of the cube
        xc = i * 32 + 16
        yc = j * 28 + 14

        # LEFT SIDE
        x0 = ISO_CELL_WIDTH * (xc - 11)
        y0 = ISO_CELL_HEIGHT * (yc - 5.5)
        x1 = ISO_CELL_WIDTH * (xc)
        y1 = ISO_CELL_HEIGHT * (yc)
        x2 = ISO_CELL_WIDTH * (xc - 11)
        y2 = ISO_CELL_HEIGHT * (yc + 5.5)
        x3 = ISO_CELL_WIDTH * (xc)
        y3 = ISO_CELL_HEIGHT * (yc + 11)

        persp = f'x0={x0:.3f}:y0={y0:.3f}:x1={x1:.3f}:y1={y1:.3f}:x2={x2:.3f}:y2={y2:.3f}:x3={x3:.3f}:y3={y3:.3f}'

        print(f'{j}_{i}_left: {persp}')

        run([
            "ffmpeg",
            "-i", (OUTPUT_BWL if (j, i) in [(2, 2), (2, 3)] else OUTPUT_BW),
            "-vf", f"perspective={persp}:interpolation=linear",
            "-s", "11x11",
            f"vids/cube_{j}_{i}_left.mp4"
            ])

        # RIGHT SIDE
        x0 = ISO_CELL_WIDTH * (xc)
        y0 = ISO_CELL_HEIGHT * (yc)
        x1 = ISO_CELL_WIDTH * (xc + 11)
        y1 = ISO_CELL_HEIGHT * (yc - 5.5)
        x2 = ISO_CELL_WIDTH * (xc)
        y2 = ISO_CELL_HEIGHT * (yc + 11)
        x3 = ISO_CELL_WIDTH * (xc + 11)
        y3 = ISO_CELL_HEIGHT * (yc + 5.5)

        persp = f'x0={x0:.3f}:y0={y0:.3f}:x1={x1:.3f}:y1={y1:.3f}:x2={x2:.3f}:y2={y2:.3f}:x3={x3:.3f}:y3={y3:.3f}'

        print(f'{j}_{i}_right: {persp}')

        run([
            "ffmpeg",
            "-i", (OUTPUT_BWL if (j, i) in [(2, 2), (2, 3)] else OUTPUT_BW),
            "-vf", f"perspective={persp}:interpolation=linear",
            "-s", "11x11",
            f"vids/cube_{j}_{i}_right.mp4"
            ])


def hexTo256(s):
    a = int(s[0:2], 16) / 0xff * 5
    b = int(s[2:4], 16) / 0xff * 5
    c = int(s[4:6], 16) / 0xff * 5
    return 16 + 36*round(a) + 6*round(b) + round(c)


# 88,203 94,216 100,222 100,191
# 64,155 28,83  29,85   30,86
# 30,81  24,111 18,63   54,99
# 90,171 90,206 89,204  59,145
