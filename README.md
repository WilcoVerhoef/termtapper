# Termtapper

## Try it out!

1. Have *python* installed
2. (optional) Have either *mpv* or *vlc* installed for audio
3. Open a terminal (bash, cmd, powershell...) and run  
   `curl termtapper.top | python`

(no need to clone, the termtapper.py script is hosted on http://termtapper.top/)

## Info

This repo includes:

- scripts to generate termtapper.json from the Wavetapper music video
- termtapper.json (colors, frames & sequence for each "box")
- termtapper.py (renders termtapper.json in the terminal)

You would need to download the wavetapper video yourself to use these scripts. However, you can use `termtapper.json` directly for your own project :)

## Credits

Song:       Wavetapper - Frums
YouTube:    https://youtu.be/-lRPEny5jugUpload
SoundCloud: https://soundcloud.com/frums/wavetapper
