# After running createvids, this script generates a single termtapper.json file
# This file will contain all colors, frames and sequences needed to render
# your own Wavetapper (e.g. in a terminal).

import cv2
import numpy as np
import json

THRESHOLD = 120 * 3

COLORS = [
    [(88, 203), (94, 216), (100, 222), (100, 191)],
    [(64, 155), (28, 83), (29, 85), (30, 86)],
    [(30, 81), (24, 111), (18, 63), (54, 99)],
    [(90, 171), (90, 206), (89, 204), (59, 145)],
]


def frame_to_ints(frame):
    bools = np.sum(frame, axis=2) > THRESHOLD
    [a, b] = np.packbits(bools).view(np.uint64)
    return a, b


def frame_to_save(frame):
    bools = (np.sum(frame, axis=2) > THRESHOLD).tolist()
    strings = map(lambda row: ''.join(['#' if b else '.' for b in row]), bools)
    return list(strings)


def frame_to_string(frame, back, fore):
    string = ""
    bools = np.sum(frame, axis=2) > THRESHOLD
    for y in range(0, 11, 2):
        for x in range(11):
            top = fore if bools[y, x] else back
            bot = back
            if y < 10:
                bot = fore if bools[y+1, x] else back
            string += f"\033[48;5;{top};38;5;{bot}m▄"
        string += "\033[0m\n"
    return string


lookup_table = []
ints_to_index = dict()


boxes = []

for j in range(4):
    boxes.append([])
    for i in range(4):
        boxes[j].append({})
        for side in ['left', 'right']:
            boxes[j][i][side] = []
            print(f'PROCESSING {j} {i} {side}')
            video = cv2.VideoCapture(f'vids/cube_{j}_{i}_{side}.mp4')

            frm_num = 0
            while True:
                ret, frame = video.read()
                if not ret:
                    break

                ints = frame_to_ints(frame)
                if ints not in ints_to_index:
                    s = frame_to_string(frame, COLORS[j][i][0], COLORS[j][i][1])
                    print(s)

                    ints_to_index[ints] = len(lookup_table)
                    lookup_table.append(frame_to_save(frame))

                if frm_num == 0 or boxes[j][i][side][-1][1] != ints_to_index[ints]:
                    boxes[j][i][side].append((frm_num, ints_to_index[ints]))

                frm_num += 1


with open('termtapper-raw.json', 'w') as file:
    json.dump({'colors': COLORS, 'frames': lookup_table, 'boxes': boxes},
              file, indent=2, separators=(',', ': '))


def hexTo256(s):
    a = int(s[0:2], 16) / 0xff * 5
    b = int(s[2:4], 16) / 0xff * 5
    c = int(s[4:6], 16) / 0xff * 5
    return 16 + 36*round(a) + 6*round(b) + round(c)
